package se.samuelandersson.pokemongo;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.joda.time.DateTime;
import org.testng.annotations.Test;

public class PokemonScannerTest
{

  @Test
  public void test()
  {

    HttpClientFactory factory = new HttpClientFactory()
    {
      @Override
      public CloseableHttpClient getCloseableHttpClient()
      {
        return new CloseableHttpClientAdapter("{\"result\":[{\"spawn_point_id\":\"46596bf5a03\",\"encounter_id\":\"1747633324811986877\",\"pokemon_id\":\"VENONAT\",\"latitude\":58.41188391680539,\"longitude\":15.748924532298792,\"expiration_timestamp_ms\":\"1474818252844\"},{\"spawn_point_id\":\"46596bf5ee9\",\"encounter_id\":\"8421390798879098189\",\"pokemon_id\":\"CHARMANDER\",\"latitude\":58.41170925397793,\"longitude\":15.745948972850561,\"expiration_timestamp_ms\":\"1474818222628\"},{\"spawn_point_id\":\"46596bf5c4d\",\"encounter_id\":\"6580153366853876573\",\"pokemon_id\":\"PIDGEOT\",\"latitude\":58.412694433875465,\"longitude\":15.746119120843526,\"expiration_timestamp_ms\":\"1474818757844\"}]}");
      }
    };

    PokemonScanner scanner = new PokemonScanner(factory);
    String content = scanner.scanArea(new Location("58.41254034450702", "15.742251875281745"));

    List<Result> results = PokemonScanner.parseResult(content, DateTime.now());

    for (Result result : results)
    {
      StringWriter writer = new StringWriter();
      try
      {
        PokemonScanner.writeCsv(writer, result, ",");
      }
      catch (IOException e)
      { // string writer doesn't throw
      }
      System.out.print(writer.toString());
    }

  }

}
