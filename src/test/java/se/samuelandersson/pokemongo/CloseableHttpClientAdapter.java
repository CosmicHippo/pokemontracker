package se.samuelandersson.pokemongo;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

@SuppressWarnings("deprecation")
public class CloseableHttpClientAdapter extends CloseableHttpClient
{
  private final String content;

  public CloseableHttpClientAdapter(String responseContent)
  {
    this.content = responseContent;
  }

  @Override
  public HttpParams getParams()
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public ClientConnectionManager getConnectionManager()
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void close() throws IOException
  {
    // TODO Auto-generated method stub

  }

  @Override
  protected CloseableHttpResponse doExecute(HttpHost target, HttpRequest request, HttpContext context)
      throws IOException, ClientProtocolException
  {
    return new CloseableHttpResponseAdapter(content);
  }

}
