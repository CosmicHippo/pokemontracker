package se.samuelandersson.pokemongo;

import org.apache.http.client.RedirectStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

/**
 * Made as a factory in order to ease testing.
 */
public interface HttpClientFactory
{
  public CloseableHttpClient getCloseableHttpClient();

  public static class Default implements HttpClientFactory
  {
    public static final HttpClientFactory INSTANCE = new Default();

    private Default()
    {
    }

    @Override
    public CloseableHttpClient getCloseableHttpClient()
    {
      final HttpClientBuilder builder = HttpClients.custom();
      return builder.build();
    }

  }
}
