package se.samuelandersson.pokemongo;

import org.joda.time.DateTime;

public class Result
{
  private DateTime dateTime;
  private String spawnPointId;
  private String encounterId;
  private String pokemonId;
  private String latitude;
  private String longitude;
  private long expirationTimestampInMs;

  public static final int LOCATION_ACCURACY = 12;

  public Result(DateTime dateTime, String spawnPointId, String encounterId, String pokemonId, String latitude,
      String longitude, long expirationTimestampInMs)
  {
    this.dateTime = dateTime;
    this.spawnPointId = spawnPointId;
    this.encounterId = encounterId;
    this.pokemonId = pokemonId;
    final String[] lat = latitude.split("\\.");
    if (lat.length == 2)
    {
      this.latitude = lat[0] + "." + lat[1].substring(0, Math.min(lat[1].length(), LOCATION_ACCURACY));
    }
    else
    {
      this.latitude = latitude;
    }
    final String[] lon = longitude.split("\\.");
    if (lon.length == 2)
    {
      this.longitude = lon[0] + "." + lon[1].substring(0, Math.min(lon[1].length(), LOCATION_ACCURACY));
    }
    else
    {
      this.longitude = longitude;
    }
    this.expirationTimestampInMs = expirationTimestampInMs;
  }

  public DateTime getDateTime()
  {
    return dateTime;
  }

  public String getSpawnPointId()
  {
    return spawnPointId;
  }

  public void setSpawnPointId(String spawnPointId)
  {
    this.spawnPointId = spawnPointId;
  }

  public String getEncounterId()
  {
    return encounterId;
  }

  public void setEncounterId(String encounterId)
  {
    this.encounterId = encounterId;
  }

  public String getPokemonId()
  {
    return pokemonId;
  }

  public void setPokemonId(String pokemonId)
  {
    this.pokemonId = pokemonId;
  }

  public String getLatitude()
  {
    return latitude;
  }

  public void setLatitude(String latitude)
  {
    this.latitude = latitude;
  }

  public String getLongitude()
  {
    return longitude;
  }

  public void setLongitude(String longitude)
  {
    this.longitude = longitude;
  }

  public long getExpirationTimestampInMs()
  {
    return expirationTimestampInMs;
  }

  public void setExpirationTimestampInMs(long expirationTimestampInMs)
  {
    this.expirationTimestampInMs = expirationTimestampInMs;
  }

  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((encounterId == null) ? 0 : encounterId.hashCode());
    result = prime * result + (int) (expirationTimestampInMs ^ (expirationTimestampInMs >>> 32));
    result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
    result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
    result = prime * result + ((pokemonId == null) ? 0 : pokemonId.hashCode());
    result = prime * result + ((spawnPointId == null) ? 0 : spawnPointId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Result other = (Result) obj;
    if (encounterId == null)
    {
      if (other.encounterId != null) return false;
    }
    else if (!encounterId.equals(other.encounterId)) return false;
    if (expirationTimestampInMs != other.expirationTimestampInMs) return false;
    if (latitude == null)
    {
      if (other.latitude != null) return false;
    }
    else if (!latitude.equals(other.latitude)) return false;
    if (longitude == null)
    {
      if (other.longitude != null) return false;
    }
    else if (!longitude.equals(other.longitude)) return false;
    if (pokemonId == null)
    {
      if (other.pokemonId != null) return false;
    }
    else if (!pokemonId.equals(other.pokemonId)) return false;
    if (spawnPointId == null)
    {
      if (other.spawnPointId != null) return false;
    }
    else if (!spawnPointId.equals(other.spawnPointId)) return false;
    return true;
  }
}