package se.samuelandersson.pokemongo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

public class PokemonScannerMain implements IValueValidator<Integer>
{
  private static final Logger log = LoggerFactory.getLogger(PokemonScannerMain.class);

  @Parameter(names = { "-h", "--help" }, help = true)
  public boolean help = false;

  @Parameter(names = { "--maxTries" }, description = "Maximum amount of tries in a row to receive results from the API (Default: 60)", validateValueWith = PokemonScannerMain.class)
  public int maxTries = 60;

  @Parameter(names = { "--tryDelay" }, description = "Delay between failed tries in ms (Default: 1000)", validateValueWith = PokemonScannerMain.class)
  public int tryDelay = 1000;

  @Parameter(names = { "--scanDelay" }, description = "Delay between scans in minutes (Default: 10)", validateValueWith = PokemonScannerMain.class)
  public int scanDelay = 10;

  @Parameter(names = { "--loops" }, description = "How many iterations to run (0 = infinite, Default: 0", validateValueWith = PokemonScannerMain.class)
  public int iterations = 0;

  @Parameter(names = { "-f", "--locationsFile" }, description = "File to read locations from")
  public File locationsFile = new File("../locations.txt");
  
  @Parameter(names = { "-o", "--outputFile" }, description = "File to write csv data to")
  public File outputFile = new File("../data.csv");

  @Override
  public void validate(String name, Integer value) throws ParameterException
  {
    if (name.contains("maxTries"))
    {
      if (value < 1)
      {
        throw new ParameterException("maxTries must be > 0");
      }
    }
    else if (name.contains("tryDelay"))
    {
      if (value < 100)
      {
        throw new ParameterException("tryDelay must be >= 100");
      }
    }
    else if (name.contains("scanDelay"))
    {
      if (value < 1)
      {
        throw new ParameterException("scanDelay must be > 0");
      }
    }
    else if (name.contains("iterations"))
    {
      if (value < 0)
      {
        throw new ParameterException("iterations must be >= 0");
      }
    }
  }

  public static void main(String[] args)
  {
    PokemonScannerMain main = new PokemonScannerMain();
    JCommander commander = new JCommander(main);

    try
    {
      commander.parse(args);

      if (main.help)
      {
        commander.usage();
        return;
      }
    }
    catch (ParameterException e)
    {
      log.error("Error parsing parameters", e);
      commander.usage();
      return;
    }

    main.execute();
  }

  private void execute()
  {
    List<Location> locationsToScan;
    try
    {
      locationsToScan = readLocations(locationsFile);
    }
    catch (Exception e)
    {
      log.error("Error reading locations file", e);
      return;
    }

    final PokemonScanner scanner = new PokemonScanner(locationsToScan, outputFile, scanDelay, tryDelay, maxTries);
    scanner.execute();
  }

  private List<Location> readLocations(File file) throws FileNotFoundException, IOException
  {
    if (file == null)
    {
      throw new NullPointerException("file");
    }

    List<Location> locations = new ArrayList<>();
    File locFile = file.getAbsoluteFile();

    try (BufferedReader reader = new BufferedReader(new FileReader(locFile)))
    {
      String line = null;
      while ((line = reader.readLine()) != null)
      {
        if (line.trim().startsWith("#"))
        {
          continue;
        }

        String[] locationString = line.split(",");
        if (locationString.length == 2)
        {
          Location location = new Location(locationString[0].trim(), locationString[1].trim());
          if (location.isValid())
          {
            if (log.isDebugEnabled())
            {
              log.debug("Adding location: {}", location);
            }
            locations.add(location);
          }
          else
          {
            log.warn("Parsed location: [{}] is not valid", location);
          }
        }
      }
    }

    return locations;
  }
}
