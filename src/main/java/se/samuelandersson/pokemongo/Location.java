package se.samuelandersson.pokemongo;

public class Location
{
  private String latitude;
  private String longitude;
  private Boolean valid = null;

  public Location(String latitude, String longitude)
  {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public String getLatitude()
  {
    return latitude;
  }

  public void setLatitude(String latitude)
  {
    this.latitude = latitude;
  }

  public String getLongitude()
  {
    return longitude;
  }

  public void setLongitude(String longitude)
  {
    this.longitude = longitude;
  }

  public boolean isValid()
  {
    if (valid == null)
    {
      double latitude = Double.parseDouble(this.latitude);
      if (latitude < -90 || latitude > 90)
      {
        return valid = false;
      }

      double longitude = Double.parseDouble(this.longitude);
      if (longitude < -180 || longitude > 180)
      {
        return valid = false;
      }
    }
    return valid = true;
  }

  @Override
  public String toString()
  {
    return String.format("Location: (Latitude: [%s], Longitude: [%s])", this.latitude, this.longitude);
  }

}
