package se.samuelandersson.pokemongo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class PokemonScanner
{
  public static final Logger log = LoggerFactory.getLogger(PokemonScanner.class);

  public static final String FPM_API = "https://api.fastpokemap.se/";
  public static final String FPM_API_WITH_ARGS = FPM_API + "?key=allow-all&ts=0&lat=%s&lng=%s";
  public static final String FPM_ORIGIN = "https://fastpokemap.se";
  public static final String FPM_AUTHORITY = "api.fastpokemap.se";

  // TODO write an algorithm that positions circles in a hexagon
  public static final double FPM_SCAN_LATITUDE_RADIUS = 0.0036; // North-south. Positive values goes to north
  public static final double FPM_SCAN_LONGITUDE_RADIUS = 0.0069; // East-west. Positive values goes to east 

  private final int maxTries;
  private final int scanDelay;
  private final int tryDelay;
  private final File outputFile;

  public static final String CSV_HEADER = "scanDatetime,encounterId,spawnPointId,pokemonId,latitude,longitude,expirationTime" +
                                          System.lineSeparator();

  private final Set<Result> encounters = new HashSet<>();
  private final List<Location> locationsToScan = new ArrayList<>();

  private CloseableHttpClient client;

  public PokemonScanner(HttpClientFactory factory)
  {
    this(factory, new ArrayList<>(), new File("data.csv"));
  }

  public PokemonScanner(List<Location> locationsToScan, File outputFile)
  {
    this(HttpClientFactory.Default.INSTANCE, locationsToScan, outputFile);
  }

  public PokemonScanner(List<Location> locationsToScan, File outputFile, int scanDelay, int tryDelay, int maxTries)
  {
    this(HttpClientFactory.Default.INSTANCE, locationsToScan, outputFile, scanDelay, tryDelay, maxTries);
  }

  public PokemonScanner(HttpClientFactory factory, List<Location> locationsToScan, File outputFile)
  {
    this(factory, locationsToScan, outputFile, 10, 1000, 60);
  }

  /**
   * 
   * @param factory
   * @param locationsToScan
   * @param scanDelay delay in minutes
   * @param tryDelay delay in ms
   * @param maxTries
   */
  public PokemonScanner(HttpClientFactory factory, List<Location> locationsToScan, File outputFile, int scanDelay,
      int tryDelay, int maxTries)
  {
    this.client = factory.getCloseableHttpClient();
    this.locationsToScan.addAll(locationsToScan);
    this.scanDelay = scanDelay * 60 * 1000;
    this.tryDelay = tryDelay;
    this.maxTries = maxTries;
    this.outputFile = outputFile;
  }

  @Override
  protected void finalize() throws Throwable
  {
    if (client != null)
    {
      client.close();
      client = null;
    }
  }

  public void execute()
  {
    if (locationsToScan.isEmpty())
    {
      log.error("No locations to scan.");
      return;
    }

    try
    {
      while (true)
      {
        for (Location loc : locationsToScan)
        {
          String content = scanArea(loc);

          if (content == null || !content.startsWith("{\"result\":"))
          {
            if (log.isDebugEnabled())
            {
              log.warn("No content received: " + content);
            }
            else
            {
              log.warn("No content received");
            }

            continue;
          }

          log.info("Received content: " + content);

          final List<Result> results = parseResult(content, DateTime.now());

          List<Result> resultsToWrite = new ArrayList<>();
          for (Result result : results)
          {
            if (!encounters.contains(result))
            {
              encounters.add(result);
              resultsToWrite.add(result);
            }
          }

          try
          {
            writeToCsv(outputFile, resultsToWrite, ",");
          }
          catch (IOException e)
          {
            log.error("Error writing to CSV file", e);
          }
        }

        try
        {
          log.info("Sleeping for " + scanDelay / 1000 + " seconds...");
          Thread.sleep(scanDelay);
        }
        catch (InterruptedException e)
        {
        }
      }
    }
    finally
    {
      if (this.client != null)
      {
        try
        {
          this.client.close();
        }
        catch (IOException e)
        {
          log.error("Error closing client", e);
        }
        finally
        {
          this.client = null;
        }
      }
    }
  }

  public static void writeCsv(Writer writer, Result result, String delimiter) throws IOException
  {
    writer.write(result.getDateTime().toString());
    writer.write(delimiter);
    writer.write(result.getEncounterId());
    writer.write(delimiter);
    writer.write(result.getSpawnPointId());
    writer.write(delimiter);
    writer.write(result.getPokemonId());
    writer.write(delimiter);
    writer.write(result.getLatitude());
    writer.write(delimiter);
    writer.write(result.getLongitude());
    writer.write(delimiter);
    writer.write(new DateTime(result.getExpirationTimestampInMs()).toLocalTime().toString());
    writer.write(System.lineSeparator());
  }

  private static void writeToCsv(File file, List<Result> results, String delimiter) throws IOException
  {
    if (!file.exists())
    {
      file.createNewFile();
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true)))
      {
        writer.write(CSV_HEADER);
      }
    }

    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true)))
    {
      for (Result result : results)
      {
        writeCsv(writer, result, delimiter);
      }
    }
  }

  private static boolean isAnyNull(Object... nullables)
  {
    for (Object o : nullables)
    {
      if (o == null)
      {
        return true;
      }
    }

    return false;
  }

  public static List<Result> parseResult(String content, DateTime dateTime)
  {
    if (log.isTraceEnabled())
    {
      log.trace("Parsing result...");
    }

    JsonElement root = new JsonParser().parse(content);

    JsonElement result = root.getAsJsonObject().get("result");
    JsonArray resultArray = result.getAsJsonArray();

    List<Result> results = new ArrayList<>();
    for (JsonElement element : resultArray)
    {
      JsonObject elementObject = element.getAsJsonObject();
      JsonElement spawnPointId = elementObject.get("spawn_point_id");
      JsonElement encounterId = elementObject.get("encounter_id");
      JsonElement pokemonId = elementObject.get("pokemon_id");
      JsonElement latitude = elementObject.get("latitude");
      JsonElement longitude = elementObject.get("longitude");
      JsonElement expirationTimestampInMs = elementObject.get("expiration_timestamp_ms");

      if (isAnyNull(spawnPointId, encounterId, pokemonId, latitude, longitude, expirationTimestampInMs))
      {
        log.warn("Received unknown format for result for: " + content);
        continue;
      }

      results.add(new Result(dateTime,
                             spawnPointId.getAsString(),
                             encounterId.getAsString(),
                             pokemonId.getAsString(),
                             latitude.getAsString(),
                             longitude.getAsString(),
                             expirationTimestampInMs.getAsLong()));
    }

    if (log.isDebugEnabled())
    {
      log.debug("Returning {} results...", results.size());
    }

    return results;
  }

  public String scanArea(Location location)
  {
    int tryCounter = 0;
    String content = null;

    while (content == null || content.startsWith("{\"error"))
    {
      if (tryCounter >= maxTries)
      {
        log.error("Max tries reached.");
        return null;
      }

      if (log.isDebugEnabled())
      {
        log.debug("Requesting contents... " + tryCounter);
      }
      content = getContentFromScan(location.getLatitude(), location.getLongitude());
      tryCounter++;
      try
      {
        Thread.sleep(tryDelay);
      }
      catch (InterruptedException e)
      {
      }
    }

    return content;
  }

  @SuppressWarnings("resource")
  private String getContentFromScan(final String lat, final String lng)
  {
    HttpGet httpGet = new HttpGet(String.format(FPM_API_WITH_ARGS, lat, lng));
    httpGet.setHeader("origin", FPM_ORIGIN);
    httpGet.setHeader("authority", FPM_AUTHORITY);

    CloseableHttpResponse response = null;
    try
    {
      try
      {
        if (log.isTraceEnabled())
        {
          log.trace("Executing request to " + httpGet.getURI());
        }

        final long measurePoint = System.currentTimeMillis();
        response = client.execute(httpGet);
        final long reponseTime = System.currentTimeMillis() - measurePoint;
        if (log.isTraceEnabled())
        {
          log.trace("Received response in " + reponseTime + " ms with status code: " +
                    response.getStatusLine().getStatusCode());
        }

        if (response.getStatusLine().getStatusCode() != 200)
        {
          log.warn("Status code: " + response.getStatusLine().getStatusCode() + ": " +
                   response.getStatusLine().getReasonPhrase());
          if (log.isDebugEnabled())
          {
            log.debug("Received headers:");
            for (Header header : response.getAllHeaders())
            {
              log.debug(header.getName() + ": " + header.getValue());
            }
          }

          return null;
        }

        HttpEntity entity = response.getEntity();
        InputStream is = entity.getContent();
        String content = null;

        if (log.isTraceEnabled())
        {
          log.trace("Reading from inputstream...");
        }

        try (Scanner s = new Scanner(is).useDelimiter("\\A"))
        {
          content = s.hasNext() ? s.next() : "";
        }

        if (log.isTraceEnabled())
        {
          log.trace("Read content from inputstream. Content null? " + (content == null ? "Yes" : "No"));
        }

        return content;
      }
      finally
      {
        if (response != null)
        {
          if (log.isTraceEnabled())
          {
            log.trace("Closing connection.");
          }

          response.close();

          if (log.isTraceEnabled())
          {
            log.trace("Connection closed.");
          }
        }
      }
    }
    catch (IOException e)
    {
      log.error("Error", e);
      return null;
    }
  }
}
